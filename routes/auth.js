const express = require('express'),
    sqlite3 = require('sqlite3'),
    config = require('config'),
    keypair = require('keypair'),
    jwt = require('jsonwebtoken'),
    router = express.Router();

/* Génération de clé privé/publique */
console.log("|| Generation of a public and a private key...")
let pairKey = keypair();

/* Connexion a la base de donnée */
let db = new sqlite3.Database('./db.sqlite3', (err) => {
    if (err){
        console.error(err.message);
    }
    console.log('|| Connected to the database');
});


router.get('/login', async(req, res) => {
    res.setHeader('Content-type', 'application/json');
    res.setHeader("Access-Control-Allow-Origin", config.get('clientUrl'));
    /* Vérification du Basic auth */
    if (!req.headers.authorization || req.headers.authorization.indexOf('Basic ') === -1) {
        return res.status(401).json({ message: 'Missing Authorization Header' });
    }

    /* Récupération des informations de connexion email/password */
    const base64Credentials =  req.headers.authorization.split(' ')[1];
    const credentials = Buffer.from(base64Credentials, 'base64').toString('ascii');
    const [email, password] = credentials.split(':');

    try{
        /* Vérifier si le couple email/password est valide */
        let sql = `SELECT * FROM user WHERE email = ? AND password = ?`;
        db.get(sql, [email,password],(err, row) => {
            if(typeof row !== 'undefined'){
                let payload = {
                    id: row.id,
                    name: row.name,
                    email: row.email,
                };
                let signOptions = {
                    algorithm: 'RS256',
                    issuer: config.get('jwtIssuer'),
                    subject: row.email,
                    audience: 'monapp',
                    expiresIn: '4h',
                };
                /* Généré un jwt */
                let token = jwt.sign(payload, {key: pairKey.private}, signOptions);
                /* Renvoyer le token */
                return res.status(200).json({ token: token });
            } else {
                return res.status(400).json({ message: 'Incorrect user information' });
            }
        });
    } catch (e) {
        return res.status(500).json({ message: 'Internal server error' });
    }
});

/* Renvoie une clé publique pour permettre au serveur applicatif de verifier la signature des tokens */
router.get('/publickey', async(req, res) => {
    res.setHeader('Content-type', 'application/json');
    return res.status(200).json({ publicKey: pairKey.public });
});

module.exports = router;
