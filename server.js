const express = require('express'),
    app = express(),
    config = require('config'),
    authRoute = require('./routes/auth');

const port = config.get('port');
var cors = require('cors');

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cors({origin: config.get('clientUrl')}));
app.use('/auth', authRoute);
app.listen(port);


console.log('|| Authorization API server started on : ' + port);
